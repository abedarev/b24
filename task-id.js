// ==UserScript==
// @name         Task Id
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @include      https://bfgsu.bitrix24.ru/*
// @grant        none
// ==/UserScript==

(function() {
	'use strict';
	let els = document.querySelectorAll('.tasks-kanban-item-title, .task-title');
	[].forEach.call(els, (el) => {
		let id = el.href.match(/([0-9]+)(\/)?$/)[1];
		el.innerHTML = "["+ id +"] " + el.innerHTML;
	});
})();
